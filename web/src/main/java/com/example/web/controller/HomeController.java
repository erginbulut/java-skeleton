package com.example.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;

@Controller
public class HomeController {

	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello World";

	@RequestMapping("/")
    public String index() {
        return "index";
	}

	@RequestMapping("/test")
    public String test(Model model) {
        model.addAttribute("name", message);
        return "index";
	}

}