package com.example.api.controller;

import com.example.api.model.SampleModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping("/")
    public SampleModel index() {
        return new SampleModel("Hello world", 1);
    }
}